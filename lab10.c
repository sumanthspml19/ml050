#include<stdio.h>
void input(int *a,int *b)
{
 printf("enter two numbers\n");
 scanf("%d%d",a,b);
}
int to_find_sum(int a,int b)
{
 int sum;
 sum=(a)+(b);
 printf("%d + %d=%d\n",a,b,sum);
 return sum;
}
int to_find_sub(int a,int b)
{
 int sub;
 sub=(a)-(b);
 printf("%d - %d=%d\n",a,b,sub);
 return sub;
}
int to_find_mul(int a,int b)
{
 int mul;
 mul=(a)*(b);
 printf("%d * %d=%d\n",a,b,mul);
 return mul;
}
int to_find_divident(int a,int b)
{
 int divident;
 divident=(a)/(b);
 printf("the divident when %d is divided by %d is %d\n",a,b,divident);
 return divident;
}
int to_find_rem(int a,int b)
{
 int rem;
 rem=(a)%(b);
 printf("the remainder when %d is divided by %d =%d\n",a,b,rem);
 return rem;
}
int main()
{
 int a,b,sum,sub,mul,divident,rem;
 input(&a,&b);
 sum=to_find_sum(a,b);
 sub=to_find_sub(a,b);
 mul=to_find_mul(a,b);
 divident=to_find_divident(a,b);
 rem=to_find_rem(a,b);
 return 0;
}