#include<stdio.h>
int input_n()
{
 int n;
 printf("enter the value of n\n");
 scanf("%d",&n);
 return n;
}
void input_array(int n,int a[n])
{
 for(int i=0;i<n;i++)
 {
  printf("enter the %d value\n",i);
  scanf("%d",&a[i]);
 }
}
float to_find_sum(int n,int a[n])
{
 float sum=0.0;
 int i;
 for(i=0;i<n;i++)
 {
  sum+=a[i];
 }
 return sum;
}
float to_find_average(int n,int a[n])
{
 float sum;
 sum=to_find_sum(n,a);
 float average;
 average=(sum/n);
 return average;
}
void output(int n,float average)
{
 printf("the average of %d numbers is %f\n",n,average);
}
int main()
{
 int n;
 float sum,average;
 n=input_n();
 int a[n];
 input_array(n,a);
 sum=to_find_sum(n,a);
 average=to_find_average(n,a);
 output(n,average);
 return 0;
}