#include <stdio.h>
#include <math.h>
struct student
{
    int roll_number;
    char name[30];
    char section[3];
    char department[30];
    float fees;
    float marks;
};
typedef struct student Student;
Student input()
{
    Student s;
    printf("Enter roll number \n");
    scanf("%d",&s.roll_number);
    printf("Enter name  \n");
    scanf("%s",s.name);
    printf("Enter section \n ");
    scanf("%s",s.section);
    printf("Enter department \n  ");
    scanf("%s",s.department);
    printf("Enter fees \n");
    scanf("%f",&s.fees);
    printf("Enter marks \n");
    scanf("%f",&s.marks);
    return s;
}
void output(Student s1,Student s2)
{
  if(s1.marks>s2.marks)
  {
    printf("The marks of %s is highest and his details are\n",s1.name);
    printf(" roll number = %d\n",s1.roll_number);
    printf("name=%s\n",s1.name);
    printf(" section = %s\n ",s1.section);
    printf("department= %s\n",s1.department);
    printf(" fees = %f \n",s1.fees);
    printf(" marks %f \n",s1.marks);
  }    
  else
    {
      printf("The marks of %s is highest and his details are\n",s2.name);
      printf(" roll number = %d\n",s2.roll_number);
      printf("name=%s\n",s2.name);    
      printf(" section = %s\n ",s2.section);
      printf("department=%s\n",s2.department);   
      printf(" fees = %f \n",s2.fees);
      printf(" marks %f \n",s2.marks);

        
    }
    
}

int main(void)
{
    Student s1,s2;
    printf("Enter the details of Student1\n");
    s1=input();
    printf("Enter the details of Student2\n");
    s2=input();
    output(s1,s2);
    return 0;
}

