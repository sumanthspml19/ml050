#include <stdio.h>
#include <math.h>
struct roots
{
    float real;
    float imag;
};
typedef struct roots roots;
void input(float *a,float *b,float *c)
{
    printf("Enter the values of the coefficients in decreasing order of variables\n");
    scanf("%f %f %f",a,b,c);
}

void find_roots(float a,float b,float c,roots *r1,roots *r2,float d)
{
   
    (*r1).real=((-b)/(2*a));
    (*r1).imag=(pow((d),0.5)/(2*a));
    (*r2).real=((-b)/(2*a));
     (*r2).imag=(pow((-d),0.5)/(2*a));
}
void output(float a,float b,float c,roots r1,roots r2,float d)
{
    
    
    if(d>0)
    {
        printf("The roots are real and distinct\n");
        
        printf("The roots are=%f and %f\n",(r1.real+r1.imag),(r1.real-r1.imag));
    }
    else if(d==0)
    {
        printf("The roots are real and equal\n");
        
        printf("The roots are=%f and %f\n",r1.real,r2.real);
    }
    else
    {
        printf("The roots are imaginary\n");
        
        printf("The roots are=%f+%fi and %f-%fi\n",r1.real,r2.imag,r2.real,r2.imag);
    }
        
}
int main()
{
    
    float real,imag;
    
    roots r1,r2;
    float a,b,c,d;
    input(&a,&b,&c);
    d=((b*b)-(4*a*c));
    find_roots(a,b,c,&r1,&r2,d);
    output(a,b,c,r1,r2,d);
    return 0;
}