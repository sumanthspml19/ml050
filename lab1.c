#include<stdio.h>
#include<math.h>
float input()
{
 float a;
 scanf("%f",&a);
 return a;
} 
float to_find_simple_interest(float p,float t,float r)
{
 int si;
 si=(p*t*r)/100;
 return si;
 }
void output(float p,float t,float r,float s)
{
 printf("the Simple Interest for a Principle %f for time %f at a rate %f is %f\n",p,t,r,s);
}
int main()
{
 float p, t, r, si;
 printf("Enter the Principle Value");
 p=input();
 printf("Enter the Time Period");
 t=input();
 printf("Enter the Rate of Interest");
 r=input();
 si=to_find_simple_interest(p,t,r);
 output(p,t,r,si);
 }
 
